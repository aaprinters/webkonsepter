<div id="col-left" class="innerContent col_left ui-resizable col-md-6 displayImportantTimed">
    <div class="col-inner bgCover  noBorder borderSolid border3px cornersAll radius0 shadow0 P0-top P0-bottom P0H noTopMargin load">
        <div class="de elImageWrapper de-image-block elAlign_center elMargin0 ui-droppable de-editable" id="tmp_image-95065" style="margin-top: 30px; outline: none; display: block; cursor: pointer;">
            <img src="//kampanje.webkonsepter.no/hosted/images/1d/13a3809e2e11e7b6d139d058891649/Higher-Res-logo-color.png" class="elIMG ximg" alt="" width="200">
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="headline-83037" style="margin-top: 45px; outline: none; cursor: pointer; font-family: &quot;Noto Sans&quot;, Helvetica, sans-serif !important;" >
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0 deUppercase de3pxLetterSpacing" style="text-align: center; font-size: 16px; color: rgb(0, 0, 0);" >
                <i class="fa_prepended fa fa-film" contenteditable="false"></i><font color="#ffffff"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">videoKurs</font></font>
                </b>
                </font>
            </div>
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="tmp_headline1-31677">
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0" style="text-align: center; font-size: 22px;"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Layout of content structure</font></font></b></div>
        </div>
        <div class="de elImageWrapper de-image-block elMargin0 ui-droppable elAlign_center de-editable" id="tmp_image-43986">
            <img src="//kampanje.webkonsepter.no/hosted/images/d5/cc00609e9d11e7a6bde568d9d6d392/bigstock--202724176.jpg" class="elIMG ximg" alt="" width="70%">
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="headline-76625" style="margin-top: 50px; outline: none; cursor: pointer; display: block; font-family: &quot;Noto Sans&quot;, Helvetica, sans-serif !important;">
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0 deUppercase de3pxLetterSpacing" style="text-align: left; font-size: 16px;"><font color="#5b5c5d"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">kurinnhold</font></font>
                </b>
                </font>
            </div>
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="tmp_paragraph-75737" style="margin-top: 15px; outline: none; cursor: pointer;">
            <div class="ne elHeadline hsSize1 lh5 elMargin0 elBGStyle0 hsTextShadow0 lh2 de1pxLetterSpacing" data-bold="inherit" style="text-align: left; color: rgb(45, 45, 45); font-size: 16px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">In this video course you will learn what a content structure is and how to create a menu.</font></font>
                <div>
                    <br>
                    <div><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">We review the most common pages on a site and you will learn how to create new pages and add these pages to the main menu.&nbsp;</font></font>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The course also takes into account how to use one of the over 20 web design templates in Divi sidebygger to get started quickly with the layout of the pages on your site.</font></font>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>