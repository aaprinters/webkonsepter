<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//do_action( 'woocommerce_before_checkout_form', $checkout );
// coupon etc


// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>
<style>
div#tmp_image-43986 {
    margin: margin-top: 30px;
    margin-top: 30px;
    outline: none;
}
div#headline-76625 {
    font-size: 18px;
    font-family: Helvetica;
    text-transform: uppercase;
}
div#col-center {
    text-align: center;
}
.woocommerce-checkout #main-content {
    background-color: rgb(240, 240, 240) !important;
}
.containerInner.ui-sortable {
    width: 100%;
    max-width: 1280px;
    margin: 0px auto;
}
.woocommerce-checkout div#left-area {
    width: 100%;
}
form.checkout.woocommerce-checkout .col-1 {
    float: left;
    width: 25%;
    background: #fff;
    padding: 100px 25px 150px;
}
form.checkout.woocommerce-checkout .col-1, form.checkout.woocommerce-checkout .col-2 {
    float: left;
    width: 25%;
    padding: 100px 25px 150px;
}
form.checkout.woocommerce-checkout .col-1 {
    background: #fff;
}
table.shop_table.woocommerce-checkout-review-order-table tfoot,div#stripe-payment-data p,.woocommerce-privacy-policy-text,li.wc_payment_method.payment_method_stripe img,#payment .methods label[for=payment_method_stripe]{
    display: none;
}
input[name="cardnumber"]:after,.payment_box.payment_method_stripe:before{
	display:none !important;
}
.woocommerce-checkout #payment ul.payment_methods,.payment_box.payment_method_stripe{
    background: #F0F0F0 !important;
	border-bottom: none;
	padding: 4px;

}
#add_payment_method #payment div.payment_box .form-row, .woocommerce-cart #payment div.payment_box .form-row, .woocommerce-checkout #payment div.payment_box .form-row {
    margin: 0;
    padding: 0;
}
i.stripe-credit-card-brand.stripe-card-brand {
    background: none !important;
}
.woocommerce form .form-row label {
    line-height: 2;
    font-family: "Noto Sans", Helvetica, sans-serif !important;
    color: rgb(45, 45, 45);
    font-size: 10px;
}
.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
    float: right;
    color: rgb(255, 255, 255);
    background-color: rgb(49, 181, 119);
    font-size: 16px;
    border: none;
    border: 1px solid rgba(0,0,0,0.1);
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2);
    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2);
    box-shadow: inset 0 1px 0 rgba(255,255,255,0.2);
    border-radius: 4px;
}
.woocommerce #payment #place_order:hover, .woocommerce-page #payment #place_order:hover {
    background-color: #289562 !important;
}
.form-row.place-order {
    background: #f0f0f0;
}
fieldset#wc-stripe-cc-form{
	margin-top: 0 !important;
}
.payment_box.payment_method_stripe {
    padding: 0 !important;
}
.woocommerce table.shop_table {
    margin-bottom: 0 !important;
}
th.product-name, th.product-total {
    font-size: 12px;
    color: rgb(45, 45, 45) !important;
}
</style>
<?php
$bootsrap_link = get_template_directory_uri().'/css/bootstrap.css';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $bootsrap_link; ?>">

<div id="col-center" class="innerContent col_left ui-resizable col-md-6 displayImportantTimed">
    <div class="col-inner bgCover  noBorder borderSolid border3px cornersAll radius0 shadow0 P0-top P0-bottom P0H noTopMargin load">
        <div class="de elImageWrapper de-image-block elAlign_center elMargin0 ui-droppable de-editable" id="tmp_image-95065" style="margin-top: 30px; outline: none; display: block; cursor: pointer;">
            <img src="//kampanje.webkonsepter.no/hosted/images/1d/13a3809e2e11e7b6d139d058891649/Higher-Res-logo-color.png" class="elIMG ximg" alt="" width="200">
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="headline-83037" style="margin-top: 45px; outline: none; cursor: pointer; font-family: &quot;Noto Sans&quot;, Helvetica, sans-serif !important;" >
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0 deUppercase de3pxLetterSpacing" style="text-align: center; font-size: 16px; color: rgb(0, 0, 0);" >
                <i class="fa_prepended fa fa-film" contenteditable="false"></i><font color="#454545" style="text-transform: uppercase;margin-left: 5px;"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">videoKurs</font></font>
                </b>
                </font>
            </div>
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="tmp_headline1-31677">
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0" style="text-align: center; font-size: 22px;color:#454545;"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Layout of content structure</font></font></b></div>
        </div>
        <div class="de elImageWrapper de-image-block elMargin0 ui-droppable elAlign_center de-editable" id="tmp_image-43986">
            <img src="//kampanje.webkonsepter.no/hosted/images/d5/cc00609e9d11e7a6bde568d9d6d392/bigstock--202724176.jpg" class="elIMG ximg" alt="" width="70%">
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="headline-76625" style="margin-top: 50px; outline: none; cursor: pointer; display: block; font-family: &quot;Noto Sans&quot;, Helvetica, sans-serif !important;">
            <div class="ne elHeadline hsSize3 lh4 elMargin0 elBGStyle0 hsTextShadow0 deUppercase de3pxLetterSpacing" style="text-align: left;font-size: 18px;text-transform: uppercase;"><font color="#5b5c5d"><b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">kurinnhold</font></font>
                </b>
                </font>
            </div>
        </div>
        <div class="de elHeadlineWrapper ui-droppable de-editable" id="tmp_paragraph-75737" style="margin-top: 15px; outline: none; cursor: pointer;">
            <div class="ne elHeadline hsSize1 lh5 elMargin0 elBGStyle0 hsTextShadow0 lh2 de1pxLetterSpacing" data-bold="inherit" style="text-align: left; color: rgb(45, 45, 45); font-size: 16px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">In this video course you will learn what a content structure is and how to create a menu.</font></font>
                <div>
                    <br>
                    <div><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">We review the most common pages on a site and you will learn how to create new pages and add these pages to the main menu.&nbsp;</font></font>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">The course also takes into account how to use one of the over 20 web design templates in Divi sidebygger to get started quickly with the layout of the pages on your site.</font></font>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
			<h3 id="order_review_heading"><?php esc_html_e( 'PAYMENT', 'woocommerce' ); ?></h3>
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				<?php //do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
